/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : DF.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 10/12/2012
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Demonstrate passing a data.frame from R to C++ and back to R.
///////////////////////////////////////////////////////////////////////////////*/

#include <vector>
#include <iostream>
#include <iomanip>

#include <Rcpp.h>
//#include "Rcpp_Extensions.hpp"

class DF{

public:

  DF( const Rcpp::DataFrame df_ ):df(df_){

    // Initialize data member of type Rcpp::DataFrame
    //df(df_);

    // Initialize data member of C++ 2D vector.
    //vec = Rcpp_Extensions::DataFrame2Vector<double>( df_ );

  }

  ~DF(){
    //df.~DataFrame();
    //delete df;
    //free( df );
    std::cout << "DF class destructor called." << std::endl;
  }

  // Rcpp::DataFrame get_df(){
  //   return df;
  // }

  // unsigned int ncol(){

  //   return Rcpp_Extensions::ncol<double>( df );

  // }

  // unsigned int nrow(){

  //   return Rcpp_Extensions::nrow<double>( df );

  // }

  // void print_vec(){

  //   for( unsigned int i = 0; i < nrow(); ++i ){

  //     for( unsigned int j = 0; j < ncol(); ++j ){
	
  // 	std::cout << std::setw(5) << vec[i][j];

  //     }

  //     std::cout << std::endl;

  //   }

  // }

  // Demonstrating passing arguments to methods.
  // This requires no additional code in the .method call.
  // double multiply( double x, double y ){

  //   return x * y;

  // }


  private:

  Rcpp::DataFrame df;
  //  std::vector< std::vector< double > > vec;

};

RCPP_MODULE( mod_df ){

  using namespace Rcpp;
  
  class_<DF>( "DF" )
    
    .constructor< const Rcpp::DataFrame >()
    //.property( "df", &DF::get_df )

    //.method( "ncol", &DF::ncol )
    //.method( "nrow", &DF::nrow )
    //.method( "print_vec", &DF::print_vec )
    //.method( "multiply", &DF::multiply, "scalar multiplication of two numbers" )
    ;
}
