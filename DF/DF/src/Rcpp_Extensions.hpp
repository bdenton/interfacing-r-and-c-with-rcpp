/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Rcpp_Extensions.hpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 10/15/2012
PROJECT    : 
DESCRIPTION: Extend Rcpp functionality
///////////////////////////////////////////////////////////////////////////////*/

#include <Rcpp.h>
#include <vector>

namespace Rcpp_Extensions{

  // Function to return number of columns in an Rcpp::DataFrame object
  template< class T >
  unsigned int ncol( const Rcpp::DataFrame& df ){

    return df.length();
    
  }

  // Function to return number of rows in an Rcpp::DataFrame object
  template< class T >
  unsigned int nrow( const Rcpp::DataFrame& df ){
    
    // Rcpp::DataFrame objects are column-major. To get number of rows
    // find the size of the first column.
    std::vector< T > row = Rcpp::as< std::vector< T > >( df[0] );
    
    return row.size();
    
  }
  
  
  // The function Rcpp::as() can only convert Rcpp objects
  // to primitive C++ types or 1D STL vectors of primitive types.
  // Converting to a 2D vector requires a little more manual work, 
  // particulalry if the 2D vector to be returned must be row-major rather than
  // column-major -- i.e the dimensions are XXX[row][column] rather than 
  // XXX[column][row].  This function takes an Rcpp::DataFrame object as its
  // argument and returns a row-major 2D vector of the specified type.
  template< class T >
  std::vector< std::vector< T > > DataFrame2Vector( const Rcpp::DataFrame& df ){

    std::vector< std::vector< T > > vec;

    std::vector< std::vector< T > > transposed;
    
    for( unsigned int j = 0; j < ncol<T>(df); ++j )
      transposed.push_back( Rcpp::as< std::vector< T > >( df[j] ) );
    
    
    std::vector< T > new_row;
    for( unsigned int i = 0; i < nrow<T>(df); ++i ){
      
      for( unsigned int j = 0; j < ncol<T>(df); ++j ){
	
	new_row.push_back( transposed[j][i] );
      
      }
      vec.push_back( new_row );
      new_row.clear();
    }

    return vec;

  }
    
    
}
