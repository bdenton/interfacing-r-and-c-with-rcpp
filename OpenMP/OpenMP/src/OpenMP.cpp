/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : OpenMP.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 05/01/2013
PROJECT    : OpenMP
DESCRIPTION: 
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <Rcpp.h>
#include <omp.h>

void print_thread( unsigned int num_threads ){
  
  unsigned int n, i;

  #if defined ( _OPENMP )
    
    omp_set_num_threads( num_threads );

    #pragma omp parallel shared(n), private(i)
    {
      n = omp_get_num_threads();
      i = omp_get_thread_num();

      #pragma omp critical
        //  Attmepting to print using Rcpp::Rcout generates the following error:
        //   "Error: C stack usage is too close to the limit"
        //  It seems Rcpp is not well-supported with OpenMP. It is advisable to
        //  avoid putting Rccp code inside omp parallel regions, if possible.

        //Rcpp::Rcout << "This is thread " << i << "." << std::endl; // Do not use. See above.

        std::cout << "This is thread " << i << "." << std::endl;
    }
  
  #endif
  
  Rcpp::Rcout << std::endl << "Total number of threads: " << n << std::endl;
}

void parallel_for(){

  std::vector< unsigned long int > a, b, c;

  unsigned long int N = 1E8;

  a.resize(N);
  b.resize(N);
  c.resize(N);

  for( unsigned long int i = 0; i < N; i++ ){
    a[i] = i;
    b[i] = i + 100;
  }
  
  omp_set_num_threads( 4 );

 #pragma omp parallel for schedule( dynamic ), shared(N)
  for( unsigned long  int i = 0; i < N; ++i ){
    c[i] = a[i] + b[i];
  }

  for( unsigned long int i = N-1; i > N-10; i-- )
    Rcpp::Rcout << c[i] << std::endl;

}


  std::vector< unsigned long int > parallel( unsigned long int N ){

  std::vector< unsigned long int > a;
  a.resize( N );

  unsigned int t_id;

  omp_set_num_threads( 4 );

#pragma omp parallel private( t_id )
  {
    t_id = omp_get_thread_num(); 

    a[t_id] = t_id;
  }

  return a;

}

RCPP_MODULE( mod_openmp ){

  using namespace Rcpp;

  function( "print_thread", print_thread );
  function( "parallel_for", parallel_for );
  function( "parallel", parallel );

}

// END OF FILE
