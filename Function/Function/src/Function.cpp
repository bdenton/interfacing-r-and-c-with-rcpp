/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Function.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 05/01/2013
PROJECT    : Function
DESCRIPTION: 
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <Rcpp.h>

Rcpp::NumericVector rcpp_f( Rcpp::Function FUN, Rcpp::NumericVector x ){

  return FUN(x);

}



RCPP_MODULE( mod_function ){

  using namespace Rcpp;

  function( "rcpp_f", rcpp_f );

}

// END OF FILE
