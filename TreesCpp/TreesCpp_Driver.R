#!/usr/bin/R
#################################################################################
# FILENAME   : TreesCpp_Driver.R
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 09/25/2013
# PROJECT    : Programming Practice - Rcpp
# DESCRIPTION: Call R tree growing algorithms from C++
#################################################################################

require( TreesCpp )

require( rpart )
require( party )

N <- 200
df <- data.frame( runif( n = N ) )
names( df ) <- "x1"
df$x2 <- runif( n = N )
df$trt <- sample( c(0,1), size = N, replace = TRUE )
df$y <- ifelse( df$trt == 1, df$x1 + 2*df$x2 + runif(1), -df$x1 - 2*df$x2 + runif(1) )

COVARS <- names(df)[!(names(df) %in% c("y","trt"))]

#################################################################################
# rpart( formula, data, weights, subset, na.action = na.rpart, method,          #
#        model = FALSE, x = FALSE, y = TRUE, parms, control, cost, ... )        #
#################################################################################

RPART_CONTROL <- rpart.control()
RPART_CONTROL$maxdepth <- 3

rpart0 <- rpart( 'y ~ x1 + x2', data = df, control = RPART_CONTROL )
rpart1 <- rpart_wrapper( response = df$y,
                         covariates = subset( df, select = COVARS ),
                         control = RPART_CONTROL )

#################################################################################
# mob( formula, weights, data = list(), na.action = na.omit,                    #
#      model = glinearModel, control = mob_control(), ... )                     #
#################################################################################

mob0 <- mob( as.formula('y ~ x1 + x2 | trt'), data = df, model = linearModel )

mob1 <- mob_wrapper( response = df$y,
                     covariates = subset( df, select = COVARS ),
                     partition_vars = subset( df, select = "trt" ) )

#################################################################################
# ctree( formula, data, subset = NULL, weights = NULL,                          #
#        controls = ctree_control(), xtrafo = ptrafo, ytrafo = ptrafo,          #
#        scores = NULL )                                                        #
#################################################################################

CTREE_CONTROL <- ctree_control()
CTREE_CONTROL@tgctrl@maxdepth <- as.integer(3)  # nested S4 classes with data type validation

ctree0 <- ctree( as.formula('y ~ x1 + x2'), data = df, controls = CTREE_CONTROL )

ctree1 <- ctree_wrapper( response = df$y,
                         covariates = subset( df, select = COVARS ),
                         controls = CTREE_CONTROL )

## END OF FILE
