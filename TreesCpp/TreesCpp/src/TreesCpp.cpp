/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : TreesCpp.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/25/2013
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Call R tree growing algorithms from C++
///////////////////////////////////////////////////////////////////////////////*/

#include <string>
#include <Rcpp.h>

/////////////////////////////////////////////////////////////////////////////////
// rpart(formula, data, weights, subset, na.action = na.rpart, method,         //
//       model = FALSE, x = FALSE, y = TRUE, parms, control, cost, ...)        //
/////////////////////////////////////////////////////////////////////////////////

SEXP rpart_cpp( Rcpp::Function RPART,
                std::string formula,
                Rcpp::DataFrame data,
                Rcpp::NumericVector weights,
                SEXP subset,
                Rcpp::Function na_action,
                std::string method,
                bool model,
                bool x,
                bool y,
                SEXP parms,
                SEXP control,
                Rcpp::NumericVector cost ){

  SEXP rpart__ = RPART( formula, data, weights, subset, na_action, method, model, x, y, parms, control, cost );

  return rpart__;

}



/////////////////////////////////////////////////////////////////////////////////
// mob(formula, weights, data = list(), na.action = na.omit,                   //
//     model = glinearModel, control = mob_control(), ...)                     //
/////////////////////////////////////////////////////////////////////////////////

SEXP mob_cpp( Rcpp::Function MOB,
              SEXP formula,
              Rcpp::IntegerVector weights,
              Rcpp::DataFrame data,
              Rcpp::Function na_action,
              SEXP model,
              SEXP control ){

  SEXP mob__ = MOB( formula, weights, data, na_action, model, control );

  return mob__;

}

/////////////////////////////////////////////////////////////////////////////////
// ctree(formula, data, subset = NULL, weights = NULL,                         //
//      controls = ctree_control(), xtrafo = ptrafo, ytrafo = ptrafo,          //
//      scores = NULL)                                                         //
/////////////////////////////////////////////////////////////////////////////////


SEXP ctree_cpp( Rcpp::Function CTREE,
                SEXP formula,
                Rcpp::DataFrame data,
                SEXP subset,
                Rcpp::IntegerVector weights,
                SEXP control,
                SEXP xtrafo,
                SEXP ytrafo,
                SEXP scores ){

  SEXP ctree__ = CTREE( formula, data, subset, weights, control, xtrafo, ytrafo, scores );

  return ctree__;

}




RCPP_MODULE( mod_trees_cpp ){

  using namespace Rcpp;

  function( "rpart_cpp", rpart_cpp );
  function( "mob_cpp", mob_cpp );
  function( "ctree_cpp", ctree_cpp );
}

// END OF FILE
