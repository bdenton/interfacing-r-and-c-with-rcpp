/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Exceptions.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 06/04/2013
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Example using exceptions rather than assertions to terminate
             program execution in Rcpp projects.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <Rcpp.h>
#include <assert.h>
#include <exception>

extern "C" {  


  // This function triggers and assertion error
  void asserts(){
    
    Rcpp::Rcout << "A failed assert ends the R session. " << std::endl;
  
    assert( 1 == 0 );
  
  }

  
  // This function throws an exception
  void exceptions(){
    
    Rcpp::Rcout << "Throwing an exception stops execution of the current "
		<< "process, but returns control back to the user at the R "
		<< "console." << std::endl <<std::endl;
    
    try{
      throw std::logic_error( "Test Exception" ); //logic_error is one type of C++ exception
    }
    catch( std::exception& ex ){
      forward_exception_to_r( ex ); // print exception message to R console
    }

    Rcpp::Rcout << "This code is never executed because program flow has "
		<< "exited this function and control has passed back to the "
		<< "R console.";
  }

}

// END OF FILE
