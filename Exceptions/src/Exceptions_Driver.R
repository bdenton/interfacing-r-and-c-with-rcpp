#!/usr/bin/R
#################################################################################
# FILENAME   : Exceptions_Driver.R
# AUTHOR     : Brian Denton <brian.denton@gmail.com>
# DATE       : 06/04/2013
# PROJECT    : Programming Practice - Rcpp
# DESCRIPTION: Example using exceptions rather than assertions to terminate
#              program execution in Rcpp projects.
#################################################################################

dyn.load( 'Exceptions.so' )
is.loaded( 'asserts' )
is.loaded( 'exceptions' )


# Triggers a failed assertion and the R session terminates.
#.Call( "asserts" )

# Throws an exception and control passes back to the R console.
.Call( "exceptions" )


dyn.load( 'Exception_Wrapper.so' )
is.loaded('exception_wrapper')
.Call( "exception_wrapper" )

## END OF FILE
