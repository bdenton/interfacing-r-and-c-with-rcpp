/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Exception_Wrapper.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 06/05/2013
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Wrap the try-catch logic in a convenience function.
///////////////////////////////////////////////////////////////////////////////*/

#include <iostream>
#include <string>
#include <exception>
#include <Rcpp.h>

void exception( bool logic_test, 
		const std::string& file, 
		unsigned int line,
		const std::string& msg = "logic_error exception thrown" ){

   if( !logic_test ){
    try{

      std::ostringstream line_stream;
      line_stream << line;

      throw std::logic_error( "[" + file + ":" + line_stream.str() + "] " + msg );
    }
    catch( std::exception& ex ){
      forward_exception_to_r( ex );
    }
  }
}

extern "C" {

  SEXP exception_wrapper(){

    // Pass a true logical statement to exception(). Does not throw exception.
    exception( 1 > 0, __FILE__, __LINE__, "PASS: 1 > 0" );

    // Pass a false logical statement to exception(). Does throw exception.
    exception( 1 < 0, __FILE__, __LINE__, "FAIL: 1 < 0" );

    // Use default exception message/
    //exception( 1 < 0, __FILE__, __LINE__ );

    return R_NilValue;
  }
}

// END OF FILE
