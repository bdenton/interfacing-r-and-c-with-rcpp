/*///////////////////////////////////////////////////////////////////////////////

FILENAME   : Vehicle.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 09/21/2012
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Example implementation of a Vehicle class to illustrate 
             interfacing R and C++ using the Rcpp package.

///////////////////////////////////////////////////////////////////////////////*/


#include <Rcpp.h>
#include <string>

class Vehicle{

public:

  static unsigned int number_of_vehicles;

  // Constructor
  Vehicle( std::string color_ ){
    
    color = color_;
    number_of_vehicles++;
    
  }

  // Destructor
  ~Vehicle(){

    number_of_vehicles--;

  }

  // Public member functions
  std::string getColor(){

    return color;

  }

  void setColor( std::string color_ ){

    color = color_;

  }


  void print(){
    
    Rcpp::Rcout << "Color: " << getColor() << std::endl;
  }
  
private:

  std::string color;
  
};

unsigned int Vehicle::number_of_vehicles = 0;

// Accessor function for static counter variable
unsigned int getCount(){
  return Vehicle::number_of_vehicles;
}
  

RCPP_MODULE( mod_vehicle ){

  using namespace Rcpp;
  
  class_<Vehicle>( "Vehicle" )
    
    .constructor< std::string >()

    .property( "color", &Vehicle::getColor, &Vehicle::setColor, "Vehicle color" )

    // Bind C++ methods to their aliases in R.
    // Also provide (optional) docstring.

    .method( "getColor", &Vehicle::getColor, 
	     "accessor method for color data member of Vehicle class" )

    .method( "setColor", &Vehicle::setColor, 
	     "mutator method for color data member of Vehicle class" )

    .method( "print", &Vehicle::print, "print method for Vehicle class" )
    ;


  // Functions that are not Vehicle class methods

  function( "getCount", getCount, "return number of Vehicle objects" );
}

