
#include <iostream>
#include <Rcpp.h>
#include <vector>

#include "Foo2.hpp"

class Foo{

private:

  int x;

  Foo2* f2_heap;
  Foo2 f2_stack;

  std::vector< int > args;


public:

  Foo( int x_, int y_, int z_ ) : f2_stack( z_, "stack" ){
    x = x_;

    f2_heap = new Foo2( y_, "heap" );

    args.push_back( x_ );
    args.push_back( y_ );
    args.push_back( z_ );
  }

  ~Foo(){
    std::cout << "Calling Foo destructor " << x << std::endl;

    delete f2_heap;

    args.clear();

  }

};

RCPP_MODULE( mod_foo_destructor ){

  using namespace Rcpp;
  
  class_<Foo>( "Foo" )
    
    .constructor< int, int, int >()

    ;
}
