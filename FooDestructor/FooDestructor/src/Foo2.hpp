
#include <iostream>
#include <Rcpp.h>
#include <string>

class Foo2{

private:

  int x;
  std::string alloc;

public:

  Foo2( int x_, std::string alloc_ ){
    x = x_;
    alloc = alloc_;

  }

  ~Foo2(){
    std::cout << "Calling Foo2 destructor " << x << "\t\t" << alloc << std::endl;
  }

};
