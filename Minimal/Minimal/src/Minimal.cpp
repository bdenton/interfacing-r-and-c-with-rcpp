/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Minimal.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 07/15/2013
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Example implementation of a minimal class to illustrate 
             interfacing R and C++ using the Rcpp package.
///////////////////////////////////////////////////////////////////////////////*/

#include <Rcpp.h>

class Minimal{

private:

  int x;

public:
  
  // Constructor
  Minimal( int x__ ){
    x = x__;
  }

  int get_x(){
    return x;
  }

};


// Bind C++ objects to R names
RCPP_MODULE( mod_minimal ){

  using namespace Rcpp;
  
  class_<Minimal>( "Minimal" )

    // Constructors can take up to six parameters, though this can include lists
    // so the effective number is essentially limitless.
    .constructor< int >()

  //.method( <R name>, <C++ name>, <docstring> )
    .method( "get_x", &Minimal::get_x, "Getter method for private class variable x." )
    ;
}

// END OF FILE
