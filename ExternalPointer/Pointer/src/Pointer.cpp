/*///////////////////////////////////////////////////////////////////////////////
FILENAME   : Pointer.cpp
AUTHOR     : Brian Denton <brian.denton@gmail.com>
DATE       : 10/18/2012
PROJECT    : Programming Practice - Rcpp
DESCRIPTION: Demonstrate passing a C++ pointer to R and back again.
///////////////////////////////////////////////////////////////////////////////*/

#include <vector>
#include <iostream>

#include <Rcpp.h>

Rcpp::XPtr< std::vector< int > > pointer(){

  std::vector< int >* ptr = new std::vector< int >;

  // Populate the vector ptr points to
  ptr->push_back( 1 );
  ptr->push_back( 2 );
  ptr->push_back( 3 );

  // Wrap ptr in an External Pointer called xptr
  Rcpp::XPtr< std::vector< int > > xptr( ptr );

  return xptr;

}


std::vector< int > dereference( Rcpp::XPtr< std::vector< int > > xptr ){
  return *xptr;
}

void delete_pointer_with_finalizer(){

  std::vector< int >* ptr = new std::vector< int >;

  // Populate the vector ptr points to
  ptr->push_back( 1 );
  ptr->push_back( 2 );
  ptr->push_back( 3 );

  // Wrap ptr in an External Pointer, but do this within an if-block. Thus,
  // when program execution reaches the end of the if-block the External Pointer
  // goes out of scope and is destroyed.

  if( true ){

    // The boolean second argument in the External Pointer constructor indicates
    // that a delete finalizer is to be registered for the External Pointer.
    // This means when xptr goes out of scope ptr is also freed and will be
    // destroyed when the garbage collector is called.

    // The default value for the boolean argument is true, so a finalizer is
    // always registered unless false is explicitly passed to the External
    // Pointer constructor.

    Rcpp::XPtr< std::vector< int > > xptr( ptr, true );

  }

  // Because ptr was already freed when xptr went out of scope in the previous 
  // if-block the following attempt to delete ptr results in a double free and
  // the program crashes due to a double free when the garbage collector is called.

  delete ptr;
}


void delete_pointer_without_finalizer(){

  std::vector< int >* ptr = new std::vector< int >;

  // Populate the vector ptr points to
  ptr->push_back( 1 );
  ptr->push_back( 2 );
  ptr->push_back( 3 );

  // Wrap ptr in an External Pointer called xptr
  if( true ){

    // Wrap ptr in an External Pointer but do not register a delete finalizer.

    Rcpp::XPtr< std::vector< int > > xptr( ptr, false );
  }

  delete ptr;  // no double free here because ptr was not freed when xptr goes out of scope
}



RCPP_MODULE( mod_pointer ){

  using namespace Rcpp;

  function( "pointer", pointer, "Create an External Pointer to a vector of integers." );
  function( "dereference", dereference, "Dereference the External Pointer" );

  function( "delete_pointer_with_finalizer", delete_pointer_with_finalizer );
  function( "delete_pointer_without_finalizer", delete_pointer_without_finalizer );

}

// END OF FILE
